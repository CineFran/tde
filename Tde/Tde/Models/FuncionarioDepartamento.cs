﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Tde.Models
{
    public class FuncionarioDepartamento
    {
        public int FuncionarioId { get; set; }
        public int DepartamentoId { get; set; }

        public Funcionario funcionario { get; set; }
        public Departamento departamento { get; set; }
    }
}

